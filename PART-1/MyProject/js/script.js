$(document).ready(function () {

    $(document).on("click", ".services__tab", function () {
        $(".services__tab").removeClass("active");
        $(this).addClass("active");
        $(".tabs__item")
            .removeClass("active")
            .eq($(this).index())
            .addClass("active");
    });

    $(function () {
        let maxItemCount = 12;
        let type = "all";
        let item = $(".table-item");
        item.slice(0, maxItemCount).show();
        $(".menu_item").click(function (e) {
            e.preventDefault();
            item.hide();
            type = $(this).attr("data-info");
            if (type === "all") {
                item.slice(0, maxItemCount).show();
            } else {
                item.show();
                item.not(`.${type}`).hide();
                item.filter(`.${type}`).slice(maxItemCount).hide();
            }
        });
        $(".work_btn").click(function (e) {
            e.preventDefault();
            $("#btn_plus").hide();
            $("#btn_text").hide();
            $("#loader").show();
            setTimeout(function () {
                if (maxItemCount < 36) {
                    maxItemCount += 12;
                }
                if (type === "all") {
                    item.slice(0, maxItemCount).show();
                } else {
                    item.filter(`.${type}`).slice(0, maxItemCount).show();
                }

                $("#btn_plus").show();
                $("#btn_text").show();
                $("#loader").hide();

                if (maxItemCount === 36) {
                    $(".work_btn").fadeOut();
                }
            }, getRandomInt(15, 30) * 100);
        });
    });

    function selectCurrentSmall() {
        $($(".reviews__item-img.small.active")[index - 1]).css({
            "border-color": "#18cfab",
            "align-self": "baseline",
        });
    }

    function deleteSelectCurrentSmall() {
        $($(".reviews__item-img.small.active")[index - 1]).css({
            "border-color": "rgba(31, 218, 181, 0.2)",
            "align-self": "auto",
        });
    }
});
