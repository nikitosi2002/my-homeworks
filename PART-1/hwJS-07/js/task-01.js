let array = ["hello", "world", "Kyiv", "Kharkiv", "Odessa", "Lviv"];
let parent = document.body;

function createList(array, parent) {
    let ul = document.createElement("ul");
    array.map(item => {
        let li = document.createElement("li")
        li.innerHTML = `${item}`
        ul.append(li)
    })
    parent.append(ul)
}
createList(array, parent);