//Асинхронність в js - це порядок виконання коду, тобто за допомогою асинхронності ми можемо якась частина коду буду чекати якогось результату, щоб продовжити нормальну роботу.
//Наприклад, щоб отримати дані з серверу, потрібно витратити деякий час. Щоб код зміг почекати ці дані, ми застосовуємо асинхронність.

const requestURLIp = 'https://api.ipify.org/?format=json'
const requestURLInfo = 'http://ip-api.com/'
const button = document.querySelector('#button');

button.addEventListener("click", async ()=>{
    const IP = await getUserIp();
    const adress = await getAdress(requestURLInfo, IP);
    renderInfo(adress)
});

async function getUserIp () {
    const result = await fetch(requestURLIp);
    const IP = await result.json();
    return IP.ip
}

async function getAdress(url, ip) {
    const response = await fetch(`${url}/json/${ip}?fields=continent,country,region,city,district`);
    const responseData = await response.json();
    return responseData
}

function renderInfo (adress) {
    const ul = document.createElement('ul')
    const {continent, country, region, city, district = 'not specified'} = adress
    ul.insertAdjacentHTML('afterbegin', `
        <li>
            <span>continent: ${continent}; </span>
            <span>country: ${country}; </span>
            <span>region: ${region}; </span>
            <span>city: ${city}; </span>
            <span>district: ${district}; </span>
        </li>
    `)
    document.body.append(ul)
}