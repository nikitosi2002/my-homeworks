//Прототипне наслідування працює так, що при створенні якогось одного об'єкта будуть використовуватися методи, властивості початкового об'єкта.
//super() у конструкторі класу-нащадка пишуться для того, щоб можна було викликати методи, функції класу-батька.

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name () {
        return this._name
    }
    get age () {
        return this._age
    }
    get salary () {
        return this._salary
    }

    set name (newName) {
        let arrNames = ['Nikita', 'Dima', 'Slava', 'Max', 'Igor', 'Misha', 'Liza', 'Veronika', 'Ira'];
        if (arrNames.includes(newName)){
            this._name = newName;
        } else {
            console.log("Ім'я недоступне!")
        }
    }
    set age (newAge) {
        if (newAge >= 18){
            this._age = newAge
        } else {
            console.log("Ви ще замалий")
        }
    }
    set salary (newSalary) {
        if (newSalary.length > 4){
            this._salary = newSalary
        }
    }
}

class Programmer extends Employee{
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }
    get salary () {
        return this._salary * 3
    }
}

const firstProgrammer = new Programmer('Nikita', 18, 8000, 'ukrainian');
const secondProgrammer = new Programmer('Dima', 22, 10000, 'ukrainian');

console.log(firstProgrammer.salary, firstProgrammer.name);
console.log(secondProgrammer);
