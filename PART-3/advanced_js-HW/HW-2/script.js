// конструкцію try...catch доречно викорситовувати тоді, коли ми маємо частину коду, в якій ймовірно виникне помилка. Тому, щоб код все одно міг нормально працювати,
// ми певну частину коду поміщуємо в конструкцію try...catch, також ми можемо там стилізувати error та написати там, що саме сталося сталося (Наприклад, коли
// після нас хтось працюватиме з кодом, то ця людина знатиме в чому конретно проблема та як її виправити.).

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

let ul = document.createElement('ul');
let div = document.querySelector('#root');

div.insertAdjacentElement('afterbegin', ul);

function createList (list) {
    list.forEach((item, i)=> {
        try {
            if (item.author === undefined){
                throw new Error(`book number: ${i + 1} author is missing`);
            } else if (item.name === undefined){
                throw new Error (`book number: ${i + 1} name is missing`);
            } else if (item.price === undefined){
                throw new Error(`book number: ${i + 1} price is missing`);
            } else {
                ul.insertAdjacentHTML('beforeend',`<li>book: ${item.name}, ${item.author}, ${item.price}</li>`)
            }
        } catch (err){
            console.log(err)
        }
    })
}

createList(books);