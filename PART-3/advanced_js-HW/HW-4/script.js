//AJAX це те за допомогою чого ми можемо отримувати інформацію з серверу або надсилати нову/оновлену інформацію на сервер асинхронно. Також у нас з'являється можливість
//оновити інтерфейс сторінки без оновлення самої сторніки.

const API = "https://ajax.test-danit.com/api/swapi/films"

const sendRequest = async(url) => {
    const response = await fetch(url);
    const result = await response.json();
    return result;
}

const getAllFilms = (api) => {
    sendRequest (api)
        .then((result)=> {
            result.forEach(element => {
                let name = element.name;
                const li = document.createElement('li');
                li.textContent = "Film name: " + name;
                document.getElementById('films').append(li);
                const titleFilmNumber = document.createElement('h3');
                li.prepend(titleFilmNumber);
                titleFilmNumber.textContent = "Film number: " + element.id;
                const filmInfo = document.createElement('p');
                filmInfo.textContent = 'Film description: ' + element.openingCrawl;
                li.append(filmInfo);
                const namePersons = document.createElement('h4')
                namePersons.textContent = 'Persons'
                li.append(namePersons);
                const allPersons = element.characters;
                const newAllPersons = allPersons.forEach(element => {
                    axios
                        .get(element)
                        .then(function (response) {
                            const namePer = document.createElement('li');
                            const persons = document.createElement('ul');
                            li.append(persons);
                            persons.append(namePer);
                            namePer.textContent = response.data.name;
                        })
                })
            })
        })

}

getAllFilms(API);