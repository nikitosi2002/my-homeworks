import './App.css';
import Button from "./components/Button/Button";
import React from "react";
import Modal from "./components/Modal/Modal";

const MODAL1 = 'MODAL1'
const MODAL2 = 'MODAL2'

class App extends React.Component {

    state = {
        modal: null,
        isClose: true
    }
    setActive = (type) => {
        this.setState({
            isClose: type
        })
    }
    openModal = (type) => {
        this.setState({
            modal: type,
            isClose: !this.state.isClose
        })
    }

    render() {
        const modal1 = (
            <Modal header="First Modal"
                   closeButton={true}
                   text={"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam aspernatur aut deleniti dignissimos doloribus eaque  illum ipsam nesciunt porro quae sequi sunt suscipit temporibus ut vel, veniam, voluptas."}
                   action={<button className='modal__button' onClick={() => alert('Im First modal thx bb')}>Make
                       magic</button>}
                   hidden={this.state.isClose}
                   setActive={this.setActive}
            />
        )
        const modal2 = (
            <Modal header="Second Modal"
                   closeButton={false}
                   text={"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam aspernatur aut deleniti dignissimos doloribus eaque eius id"}
                   action={<>
                       <button className='modal__button' onClick={() => alert('Im Second modal thx bb')}>Say Hi!
                       </button>
                       <button className='modal__button'
                               onClick={() => alert('U transfer all ur money to our bank-account')}>Paid
                       </button>
                   </>}
                   hidden={this.state.isClose}
                   setActive={this.setActive}
            />
        )
        return (
            <div className="App">
                <div className={'button-wrapper'}>
                    <Button content={'Open first modal'}
                            color={'#BCF8EC'}
                            handleClick={() => this.openModal('MODAL1')}
                    />
                    {this.state.modal === MODAL1 && modal1}

                    <Button content={'Open second modal'}
                            color={'#7B435B'}
                            handleClick={() => this.openModal('MODAL2')}
                    />
                    {this.state.modal === MODAL2 && modal2}
                </div>
            </div>
        )
    }
}

export default App;
