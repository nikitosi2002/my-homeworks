// 1)Описать своими словами для чего вообще нужны функции в программировании.
// Функції нам потрібні, щоб спростити читаймість коду та записавши дію, яку нам потрібно буде виконувати декілька разів в різних місцях, в одному місці.
// 2)Описать своими словами, зачем в функцию передавать аргумент.
// У функцію ми передаємо аргументи, щоб ми могли передавати дані в тіло функції та функція знала з якими даними вона працюватиме.

let userFirstNumber = prompt("Введіть перше число");
while (Number.isNaN(+userFirstNumber) || !userFirstNumber){
    userFirstNumber = prompt("Введіть перше число ще раз");
}

let userSecondNumber = prompt("Введіть друге число");
while (Number.isNaN(+userSecondNumber) || !userSecondNumber){
    userSecondNumber = prompt("Введіть друге число ще раз");
}

let userOperation = prompt("Введіть одну з операцій: +, -, *, /")

function sum(a, b) {
    return a + b;
}

function subtraction(a, b) {
    return a - b;
}

function multiply(a, b) {
    return a * b;
}

function division(a, b) {
    return a / b;
}

function count(firstNumber, secondNumber, operation) {
    switch (operation) {
        case "+":
            return sum(firstNumber, secondNumber);
            break;
        case "-":
            return subtraction(firstNumber, secondNumber);
            break;
        case "*":
            return multiply(firstNumber, secondNumber);
            break;
        case "/":
            return division(firstNumber, secondNumber);
            break;
        default:
            alert("Немає такої операції");
    }
}

console.log(count(userFirstNumber, userSecondNumber, userOperation));