// Описать своими словами в несколько строчек, зачем в программировании нужны циклы.
// Цикли нам потрібно для того, щоб виконати певну кількість раз якесь просте завдання.

let userNumber = prompt("Введіть ваше число");
while (Number.isNaN(+userNumber) || !userNumber || Number.isInteger(userNumber)){
    userNumber = prompt("Введіть ваше число ще раз");
}

for (let i = 1; i <= userNumber; i++){
    if (i % 5 === 0){
        console.log(i);
    } else if (userNumber < 5){
        alert("Sorry, no numbers");
    }
}

// Якщо рахувати від 0 (і = 0), то у нас в будь-якому випадку буде одне число - 0