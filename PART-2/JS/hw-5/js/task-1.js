// Екранування потрібне для того, щоб виводити спеціальні символи на екран \, ' і тд

function createNewUser(firstName, lastname, birthday) {
    const user = {
        firstName,
        lastname,
        birthday,
        getLogin(){
            return `${firstName[0].toLowerCase()}${lastname.toLowerCase()}`;
        },
        getAge(){
            return new Date().getFullYear() - this.birthday.slice(-4);
        },
        getPassword(){
            return `${firstName[0].toUpperCase()}${lastname.toLowerCase()}${this.birthday.slice(-4)}`;
        }
    };
    return user
}
const newUser = createNewUser(prompt("Enter your name"), prompt("Enter your lastname"), prompt("Enter your birthday dd.mm.yyyy", "dd.mm.yyyy"));
console.log(newUser, newUser.getAge(), newUser.getPassword());

