// Цикл forEach для кожного елементу масиву один за одним застосовує функцію передбачену далі в умові.

const filterBy = (array, data) => {
    let arr = array;
    let info = data;
    return arr.filter(element => typeof element !== info);
}

console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));