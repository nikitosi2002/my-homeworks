// Document Object Model (DOM) - це те як виглядає документ HTMl у вигляді дерева, також його можна змінювати.

let array = ["hello", "world", "Kyiv", "Kharkiv", "Odessa", "Lviv"];
let parent = document.body;

const showArray = (array, parent) => {
    const newArray = array.map((elem)=>{
        return `<li>${elem}</li>`
    });
    return parent.insertAdjacentHTML('afterbegin', [...newArray].join(''));
};
showArray(array, parent);