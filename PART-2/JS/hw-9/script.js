// let tab = function () {
//     let tabNav = document.querySelectorAll('.tabs-title'),
//         tabContent = document.querySelectorAll('.tab'),
//         tabName;
//
//     tabNav.forEach(item => {
//         item.addEventListener('click', selectTabNav)
//     });
//
//     function selectTabNav() {
//         tabNav.forEach(item => {
//             item.classList.remove('active');
//         });
//         this.classList.add('active');
//         tabName = this.getAttribute('data-tab');
//         selectTabContent(tabName);
//     }
//
//     function selectTabContent(tabName) {
//         tabContent.forEach(item => {
//             item.classList.contains(tabName) ? item.classList.add('active') : item.classList.remove('active');
//         })
//     }
//
// };
//
// tab();


const list = document.querySelector('.tabs'),
    items = document.querySelectorAll('.tab'),
    listItems = document.querySelectorAll('.tabs-title')

function filter() {
    list.addEventListener('click', event =>{
        const targetFilter = event.target.dataset.tab;
        const targetFilterItem = event.target;

        listItems.forEach(item =>{
            item.classList.remove('active');
        });
        targetFilterItem.classList.add('active');

        switch (targetFilter){
            case 'akali':
                getItems(targetFilter)
                break
            case 'anivia':
                getItems(targetFilter)
                break
            case 'draven':
                getItems(targetFilter)
                break
            case 'garen':
                getItems(targetFilter)
                break
            case 'katarina':
                getItems(targetFilter)
                break
        }
    });
}

filter();

function getItems(className) {
    items.forEach(item => {
        if (item.classList.contains(className)){
            item.classList.add('active');
        } else {
            item.classList.remove('active');
        }
    })
}