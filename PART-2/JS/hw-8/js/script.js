// Це функція, яка викликається, коли у нас виконалась якась подія.

const root = document.getElementById('root');

const input = document.createElement('input');
input.classList.add('input');
input.type = 'number';


const price = document.createElement('p');
price.innerHTML = 'Price';
const button = document.createElement('button');
button.innerHTML = 'Validate';
button.type = 'submit';

button.addEventListener('click', function (event) {
    let textInputValue = input.value;
    if (textInputValue < 0){
        input.style.outlineColor = `red`;
        root.insertAdjacentHTML(
            "afterend", `<p>Please enter correct price</p>`
        );
    } else {
        root.style.outlineColor = `green`;
        root.insertAdjacentHTML(
            "afterend", `<span class="list-item-text">Текущая цена: ${textInputValue} грн ${getRemoveListItemBtn()}</span>`
        );
    }
});

function removeListItem (element) {
    let isAgree = confirm('Удалить элемент');
    if (isAgree){
        element.closest('.list-item-text').remove()
    }
}

const getRemoveListItemBtn = () => {
    return `<span class="remove-button" onclick="removeListItem(this)">x</span>`
    // return `<span class="remove-button" onclick="removeListItem(event)">x</span>`
};

root.append(price, input, button);