// З занять я зрозумів, що try-catch ми використовуємо частіше всього в роботі з серверами. Але також за допомогою цього ми можеом формувати власні помилки,
// назву помилки, текст помилки. Наприклад, коли після нас хтось працюватиме з кодом, то ця людина знатиме в чому конретно проблема та як її виправити.


const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

let ul = document.createElement('ul');
const properties = ['author', 'name', 'price'];
document.querySelector('#root').insertAdjacentElement("afterbegin", ul);

function createList (list){
    list.forEach((item, i)=>{
        try{
            if (item.author === undefined){
                throw new Error(`book number ${i + 1}: author is missing`)
            } else if (item.name === undefined){
                throw new Error(`book number ${i + 1}: name of book is missing`)
            } else if (item.price === undefined){
                throw new Error(`book number ${i + 1}: price of book is missing`)
            } else {
                ul.insertAdjacentHTML("beforeend", `<li>book: ${item.name}, ${item.author}, ${item.price}</li>`)
            }
        }catch (err){
            console.log(err)
        }
    })
}

createList(books);