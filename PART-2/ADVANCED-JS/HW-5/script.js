// Асинхронність - це те, за допомогою чого ми встановлюємо певну послідовність роботи коду, тобто кажемо комп'ютеру
// дочекатись якогось моменту та продовжити оброблювати код.

const requestURLIp = 'https://api.ipify.org/?format=json'
const requestURLInfo = 'http://ip-api.com/'
const btn = document.getElementById('btn')

btn.addEventListener('click', async function (){
    const IP = await getUserIp()
    const adress = await getAdress(requestURLInfo, IP);
    console.log(adress)
    const infoForRender = renderData(adress)
})

async function getUserIp() {
    const result = await fetch(requestURLIp)
    const IP = await result.json()
    return IP.ip
}

async function getAdress(url, ip){
    const response = await fetch(`${url}/json/${ip}?fields=continent,country,region,city,district`)
    const responseData = await response.json()
    return responseData
}

function renderData(adress){
    const ul = document.createElement('ul')
    const {continent, country, region, city, district = 'not specified'} = adress

    ul.insertAdjacentHTML('beforeend', `
    <li>
        <span>continent: ${continent},</span>
        <span>country: ${country},</span>
        <span>region: ${region},</span>
        <span>city: ${city},</span>
        <span>district: ${district}.</span>
    </li>
`)
    document.body.append(ul)
}