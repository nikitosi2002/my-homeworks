class FilmsWithInfo {
    constructor(url) {
        this.url = url;
    }
    request(){
        const getResponse = new Promise((resolve) =>{
            const xhr = new XMLHttpRequest();
            xhr.open('GET', this.url);
            xhr.send();
            xhr.onload = () => resolve(JSON.parse(xhr.response));
        });
        getResponse
            .then((response) => {
                response.forEach(element => {
                    let name = element.name;
                    const li = document.createElement('li');
                    li.textContent = "Film name: " + name;
                    document.getElementById('films').append(li);
                    const titleFilmNumber = document.createElement('h3');
                    li.prepend(titleFilmNumber);
                    titleFilmNumber.textContent = "Film number: " + element.id;
                    const filmInfo = document.createElement('p');
                    filmInfo.textContent = 'Film description: ' + element.openingCrawl;
                    li.append(filmInfo);
                    const namePersons = document.createElement('h4')
                    namePersons.textContent = 'Persons'
                    li.append(namePersons);
                    const allPersons = element.characters;
                    const newAllPersons = allPersons.forEach(element => {
                        axios
                            .get(element)
                            .then(function (response) {
                                const namePer = document.createElement('li');
                                const persons = document.createElement('ul');
                                li.append(persons);
                                persons.append(namePer);
                                namePer.textContent = response.data.name;
                            })
                    })
                })
            })
    }
}

const  films = new FilmsWithInfo('https://ajax.test-danit.com/api/swapi/films');
films.request();