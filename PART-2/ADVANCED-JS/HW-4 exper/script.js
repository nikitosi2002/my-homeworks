    const requestURL = 'https://ajax.test-danit.com/api/swapi/films'

    const  renderFilmList = (data) => {
        const ul = document.createElement('ul');
        let arrCharacters = [];
        let arrNames = [];
        const arr =  data.map(element => {
            const {id, name , openingCrawl, characters} = element
            const allPersons = characters.map(element => {
                fetch(element)
                    .then(response => response.json())
                    .then(data => data)
                arrCharacters = characters
            })
            let fetchArrayCharacters = arrCharacters.map(url => fetch(url));
            Promise.all(fetchArrayCharacters)
                .then(responses => Promise.all(responses.map(r => r.json())))
                .then(data =>
                    data.forEach(e => {
                        const {name} = e;
                        arrNames = name
                    })
                );
            return `
        <li>
          <div>
            <h3>${name}</h3>
            <h3>${id}</h3>
          </div>
          <br>
          <div>
            <p>${openingCrawl}</p>
          </div>
          <h4>Character</h4>
            <p>Name: ${arrNames}</p>
          <hr>
        </li>
            `
        });
        ul.innerHTML = arr.join(' ')
        document.body.append(ul)
    }

    function getFilms() {
        fetch(requestURL)
            .then(response => response.json())
            .then(data => renderFilmList(data))

    }
    getFilms()

