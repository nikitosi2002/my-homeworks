//Прототивне наслідування працює так, що при створенні якогось одного об'єкта будуть використовуватися методи, властивості початкового об'єкта.

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name() {
        return this._name;
    }
    get age() {
        return this._age;
    }
    get salary() {
        return this._salary;
    }
    set name(newName) {
        const arrayName = ['Nikita', 'Dima', 'Slava', 'Max', 'Igor', 'Misha', 'Liza', 'Veronika', 'Ira'];
        if (arrayName.includes(newName)){
            this._name = newName;
        }
    }
    set age(newAge) {
        if (newAge >= 18){
            this._age = newAge;
        }
    }
    set salary(newSalary) {
        if (newSalary.length >= 4){
            this._salary = newSalary;
        }
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    get salary() {
        return this._salary * 3;
    }
    set salary(newSalary) {
        if (newSalary.length >= 5){
            this._salary = newSalary;
        }
    }
}

const newFirstProgrammer = new Programmer("Nikita", 18, 10000, "ukrainian");
const newSecondProgrammer = new Programmer("Dima", 21, 10500, "ukrainian");
console.log(newFirstProgrammer);
console.log(newSecondProgrammer);