const gulp = require("gulp");

const clearDistFolder = require("./gulp-tasks/cleanDist").cleanDist;
const htmlTask = require("./gulp-tasks/html").html;
const imgTask = require("./gulp-tasks/img").img;
const cssTask = require("./gulp-tasks/styles").styles;
const jsTask = require("./gulp-tasks/minsript").js;

function devTask() {
    return gulp.series(clearDistFolder, gulp.parallel(htmlTask, cssTask, imgTask, jsTask));
}

function watchFiles() {
    gulp.watch(['src/'], devTask());
}

exports.dev = devTask();
exports.watch = watchFiles;


