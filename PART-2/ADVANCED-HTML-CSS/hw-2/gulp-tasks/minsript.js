const gulp = require("gulp");
const minify = require('gulp-minify');
const rename = require("gulp-rename");

function handleJsFiles() {
    return gulp.src("src/js/script.js")
        .pipe(minify())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('build/js'))
}

exports.js = handleJsFiles;