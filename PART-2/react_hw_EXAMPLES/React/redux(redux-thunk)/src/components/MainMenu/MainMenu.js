import React from 'react';
import {NavLink} from "react-router-dom";
import './MainMenu.scss'

const MainMenu = () => {
  return (
    <div className={'menu'}>
      <ul className={'menu__list'}>
        <li className={'item'}><NavLink exact to='/products' activeClassName='selected'>Products</NavLink></li>
        <li className={'item'}><NavLink exact to='/cart' activeClassName='selected'>Cart</NavLink></li>
        <li className={'item'}><NavLink exact to='/favorites' activeClassName='selected'>Favorites</NavLink></li>
      </ul>
    </div>
  );

}

export default MainMenu;