import {render} from '@testing-library/react'
import Modal from "../Modal/Modal";
import userEvent from "@testing-library/user-event";

const modalProps = {
  header: 'Header test',
  closeButton: true,
  action: <div>Test</div>,
  text: 'Test text',
  hidden: false
}

describe('Modal tests', () => {
  test('Render modal window', () => {
    render(<Modal/>)
  })
  test('Modal render with props(header, text)', () => {
    const {getByText} = render(<Modal
      header={modalProps.header}
      text={modalProps.text}
    />)
    getByText(modalProps.header, modalProps.text)
  })
  test('Close button test', () => {
    const setActive =  jest.fn()
    const {getByTestId} = render(<Modal setActive={setActive} closeButton={modalProps.closeButton}/>)
    const closeBtn = getByTestId('close-btn')
    expect(setActive).not.toHaveBeenCalled()
    userEvent.click(closeBtn)
    expect(setActive).toHaveBeenCalledTimes(1)
  })
})