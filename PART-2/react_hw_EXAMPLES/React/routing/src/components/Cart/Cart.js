import React from 'react';
import CardList from "../CardList/CardList";
import './Cart.scss'

const Cart = () => {
  return (
    <div className={'cart'}>
      <h1 className={'cart__title'}>Cart</h1>
      <CardList itCart/>
    </div>
  );

}

export default Cart;