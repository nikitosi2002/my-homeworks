import React, {useEffect, useState} from 'react';
import Card from "../Card/Card";
import './CardList.scss'
import axios from "axios";


const CardList = ({itCart}) => {
  const [cardList, setCardList] = useState([])
  const [favorList, setFavorList] = useState([])
  const [cartList, setCartList] = useState(JSON.parse(localStorage.getItem('cartList')) || [])

  useEffect(() => {
    const list = JSON.parse(localStorage.getItem('favorList'));
    list && setFavorList(list);
  }, [])

  useEffect(() => {
    axios('/cardsList.json')
      .then(r => setCardList(r.data))
      console.log("CartList: "+  Array.isArray(cartList))
      console.log("FavorList: "+  Array.isArray(favorList))

  }, [])

  const renderProducts = () => {
    return (
    cardList.map(card =>
      <Card
        key={card.article}
        name={card.name}
        price={card.price}
        url={card.url}
        color={card.color}
        article={card.article}
        singer={card.singer}
        year={card.year}
        cardInCart={false}
        cartList={cartList}
        setCartList={setCartList}
        favorList={favorList}
        setFavorList={setFavorList}
      />
    )
    )
  }
  //
  // const cart = cardList.map(card => {
  //     console.log('INCLUDES ' + cartList.includes(card.article))
  //   // if(cartList.includes(card.article)) {
  //     return <Card
  //       key={card.article}
  //       name={card.name}
  //       price={card.price}
  //       url={card.url}
  //       color={card.color}
  //       article={card.article}
  //       singer={card.singer}
  //       year={card.year}
  //       cardInCart={true}
  //       cartList={cartList}
  //       setCartList={setCartList}
  //       favorList={favorList}
  //       setFavorList={setFavorList}
  //     />
  //   }
  //   // }
  // )

  return (
    <div className={'cardList'}>
      {!itCart && <h1 style={{color: "white"}}>Albums</h1>}
      <div className={'cardList-wrapper'}>
        {!itCart && renderProducts()}
        {/*{itCart && cart}*/}
      </div>
    </div>
  );

}

export default CardList;