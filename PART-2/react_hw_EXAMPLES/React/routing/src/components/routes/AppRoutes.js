import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import Cart from "../Cart/Cart";
import CardList from "../CardList/CardList";

const AppRoutes = () => {
  return (
    <div>
      <Switch>
        <Redirect exact from='/' to='/products'/>

        <Route exact path='/products'>
          <CardList/>
        </Route>
        <Route exact path='/cart'><Cart/></Route>
      </Switch>
    </div>
  );
}

export default AppRoutes;