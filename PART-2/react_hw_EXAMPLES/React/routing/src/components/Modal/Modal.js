import React from 'react';
import './Modal.scss'

const Modal = ({header, closeButton, action, text, hidden, setActive}) => {
  if (hidden) return null
  return (
    <div className="modal" hidden={hidden} onClick={() => setActive(true)}>
      <div className="modal__dialog" onClick={e => e.stopPropagation()}>
        <div className="modal__header">
          <h3 className="modal__title">{header}</h3>
          {closeButton && <span className="modal__close" onClick={() => setActive(true)}>X</span>}
        </div>
        <div className="modal__body">
          <div className="modal__content">{text}</div>
        </div>
        {action && <div className="modal__footer">{action}</div>}
      </div>
    </div>

  );
}

export default Modal;