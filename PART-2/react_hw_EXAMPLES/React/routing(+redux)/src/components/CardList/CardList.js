import React, {useEffect, useState} from 'react';
import Card from "../Card/Card";
import './CardList.scss'
import axios from "axios";
import {useDispatch, useSelector} from "react-redux";


const CardList = ({itCart, cardsInCart , itProducts, itFavor, cardInFavor}) => {
  const cardList  = useSelector(state => state.cardList.data)
  const dispatch = useDispatch()

  useEffect(() => {
    if(itProducts) {
      axios('/cardsList.json')
        .then(r => dispatch({type: 'SET_PRODUCTS', payload: r.data}))
        .catch(err => console.log(err))
    }
  }, [])

  const products = cardList?.map(card =>
    <Card
      key={card.article}
      name={card.name}
      price={card.price}
      url={card.url}
      color={card.color}
      article={card.article}
      singer={card.singer}
      year={card.year}
    />
  )
  const cart = cardsInCart?.map(card =>
         <Card
          key={card.article}
          name={card.name}
          price={card.price}
          url={card.url}
          color={card.color}
          article={card.article}
          singer={card.singer}
          year={card.year}
          inCart
        />
  )
  const favorites = cardInFavor?.map(card =>
    <Card
      key={card.article}
      name={card.name}
      price={card.price}
      url={card.url}
      color={card.color}
      article={card.article}
      singer={card.singer}
      year={card.year}
    />
  )
  
  return (
    <div className={'cardList'}>
      <div className={'cardList-wrapper'}>
        {itProducts && products}
        {itCart && cart}
        {itFavor && favorites}
      </div>
    </div>
  );
}

export default CardList;