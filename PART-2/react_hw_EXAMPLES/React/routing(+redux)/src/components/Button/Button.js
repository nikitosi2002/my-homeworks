import React from 'react';
import './Button.scss'

const Button = ({content, color, handleClick, article}) => {
  return (
    <button className={'open-button'}
            style={{background: color}}
            onClick={() => handleClick(article)}>
      {content}
    </button>
  );
}
export default Button;