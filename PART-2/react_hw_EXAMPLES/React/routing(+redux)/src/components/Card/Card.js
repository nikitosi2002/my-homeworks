import React, {useEffect, useState} from 'react';
import Button from "../Button/Button";
import Star from "../Star/Star";
import Modal from "../Modal/Modal";
import './Card.scss'
import {useDispatch, useSelector} from "react-redux";
import {logDOM} from "@testing-library/react";

const Card = ({
                name,
                price,
                url,
                color,
                article,
                singer,
                year,
                inCart
              }) => {
  const [isClose, setIsClose] = useState(true)
  const [inFavorites, setInFavorites] = useState(false)

  const cartList = useSelector(state => state.cartList.data,)
  const favorList = useSelector(state => state.favorList.data,)
  const dispatch = useDispatch()

  useEffect(() => {
    if (favorList.includes(article)) {
      setInFavorites(true)
    }
  }, [article])

  const openModal = () => {
    setIsClose(!isClose)
  }

  const setActive = (type) => {
    setIsClose(type)
  }

  const deleteFromCart = (item) => {
    const updCart = cartList.filter(e => e !== item)
    localStorage.setItem("cartList", JSON.stringify(updCart))
    dispatch({type: 'DELETE_FROM_CART', payload: updCart})
    setActive(true)
  }

  const toggleFavorites = (item) => {
    if (!favorList.includes(item)) {
      setInFavorites(true)
      favorList.push(item)
      localStorage.setItem(`favorList`, JSON.stringify(favorList))
      dispatch({type: 'SET_TO_FAVORITE', payload: favorList})
    } else {
      const updFavorList = favorList.filter(e => e !== item)
      setInFavorites(false)
      localStorage.setItem(`favorList`, JSON.stringify(updFavorList))
      dispatch({type: 'DELETE_FROM_FAVORITE', payload: updFavorList})
    }

  }

  const addToCart = (item) => {
    if (!cartList.includes(item)) {
      cartList.push(item)
    }
    localStorage.setItem('cartList', JSON.stringify(cartList))
    dispatch({type: 'SET_TO_CART', payload: cartList})
    setActive(true)
  }

  const modal = (
    <Modal
      header={'Adding'}
      closeButton={true}
      text={"Add this Album to cart?"}
      action={<div>
        <button className='modal__button' onClick={() => addToCart(article)}>Yes
        </button>
        <button className='modal__button' onClick={() => setActive(true)}>No</button>
      </div>}
      hidden={isClose}
      setActive={setActive}
    />)

  const delModal = (
    <Modal
      header={'Delete'}
      closeButton={true}
      text={"Deleted this Album from cart?"}
      action={<div>
        <button className='modal__button' onClick={() => deleteFromCart(article)}>Yes
        </button>
        <button className='modal__button' onClick={() => setActive(true)}>No</button>
      </div>}
      hidden={isClose}
      setActive={setActive}
    />)

  return (
    <div className={'card'} style={{background: color}}>
      <img width={250} height={250} src={url} alt=""/>
      <h4 className={"card-title"}>"{name}"</h4>
      <p className={"card-price"}>Price: {price}</p>
      <div className={'card-desc'}>
        <p>Singer: {singer}</p>
        <p>Year: {year}</p>
      </div>
      <div className={'card-options'}>
        <Star
          handleClick={() => toggleFavorites(article)} inFavorites={inFavorites}
        />
        {inCart && <Button content={'DELETE'} color={'red'} article={article} handleClick={openModal}/>}
        {!inCart && <Button content={'ADD TO CART'} color={'black'} article={article} handleClick={openModal}/>}
      </div>
      {!isClose && modal}
      {inCart && delModal}
    </div>
  );
}

export default Card;