let tab = function () {
    let tabNav = document.querySelectorAll('.tabs-title'),
        tabContent = document.querySelectorAll('.tab'),
        tabName;

    tabNav.forEach(item => {
        item.addEventListener('click', selectTabNav)
    });

    function selectTabNav() {
        tabNav.forEach(item => {
            item.classList.remove('active');
        });
        this.classList.add('active');
        tabName = this.getAttribute('data-tab');
        selectTabContent(tabName);
    }

    function selectTabContent(tabName) {
        tabContent.forEach(item => {
            item.classList.contains(tabName) ? item.classList.add('active') : item.classList.remove('active');
        })
    }

};

tab();

const list = document.querySelector('.works-list-items'),
    items = [...document.querySelectorAll('.catalog-item')],
    listItems = document.querySelectorAll('.works-list-item')

function filter() {
    list.addEventListener('click', event =>{
        const targetFilter = event.target.dataset.filter;
        const targetFilterItem = event.target;

        listItems.forEach(item =>{
            item.classList.remove('activeFilter');
        });
        targetFilterItem.classList.add('activeFilter');

        switch (targetFilter){
            case 'all':
                getItems('catalog-item')
                break
            case 'graphic':
                getItems(targetFilter)
                break
            case 'web':
                getItems(targetFilter)
                break
            case 'landing':
                getItems(targetFilter)
                break
            case 'wordpress':
                getItems(targetFilter)
                break
        }
    });
}

filter();

function getItems(className) {
    items.forEach(item=>{
        item.classList.add('hide')
    });

    items.forEach(item => {
        if (item.classList.contains(className)){
            item.classList.remove('hide');
        }
    })
}


const loadMore = document.getElementById('loadmore');
let currentItems = 12;
loadMore.addEventListener('click', (event) => {
    const elementList = [...document.querySelectorAll('.photos-list .allItems')];

    for (let i = currentItems; i < currentItems + 12; i++) {
        if (elementList[i]) {
            elementList[i].classList.remove('unvisible');
        }
    }
    currentItems += 12;

    if (currentItems >= elementList.length) {
        event.target.style.display = 'none';
    }
})



const sliderPhoto = document.getElementsByClassName('employee-photo');
const sliderPhotoArray = [...sliderPhoto];

const sliderInfo = document.getElementsByClassName('employee-info');
const sliderInfoArray = [...sliderInfo];

let i = 0;

function useSlider (event) {
    if (event.target.classList.contains('employee-photo')){
        i = sliderPhotoArray.indexOf(event.target);
    } else if (event.target.classList.contains('btn-left')){
        i === 0 ? i = 3 : i--;
    } else if (event.target.classList.contains('btn-right')){
        i === 3 ? i = 0 : i++;
    }

    sliderInfoArray.forEach(item =>{
        item.classList.remove('active');
    })
    sliderInfoArray[i].classList.add('active');

    sliderPhotoArray.forEach(item =>{
        item.classList.remove('upContent');
    })
    sliderPhotoArray[i].classList.add('upContent');
}

const catalogRoulette = document.querySelector('.block-staff-catalog-roulette');
catalogRoulette.addEventListener('click', useSlider);